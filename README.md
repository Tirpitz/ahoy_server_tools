# //Ahoyworld.co.uk presents: Ahoy Server Tools #
 
 
### What is this: ###
This is a collection of PowerShell & Batch scripts that will help you manage your dedicated gameservers on a windows machine. We use these scripts to manage our own servers, and thought to release them in the open, as they might be beneficial for other communities too.
  
### Arma3 Restarter ###
- Start & re-start Arma3 Server  
- Start & re-start BEC  
- Update and validate your Arma3 Server installation via SteamCMD  
- Sync Keys, Missions and BE-configurations across servers  
- Clean and archive log-files (Server & BEC)  
- Automatic Server Configuration & Log backup (incl BEC)  
- Supports TADST profiles  
- And more...
  
Documentation:  
https://docs.google.com/document/d/1tjpWVrarHHUY2zxQe-1HFdEyeus_aDumcN-sUYXBnmA  
  
### Squad Restarter ###
- Start & re-start Squad Server  
- Allows you to set dedicated CPU Core(s) for the Squad Server  
- Update and validate your Squad Server installation via SteamCMD  
- Automatic Server Configuration & Log backup
- And more...
  
Documentation:  
https://docs.google.com/document/d/12BuIfNB8puUpwy2OAGsJ2KApqK-DnciQNFnwSFA_t7o
  
### Archived Projects: ###
- Restarter script (Restarts Arma & BEC when crashed)  
- Updater script (Updates all your arma servers at once, using SteamCMD)  
- Backup script (Simple script that can be run to make remote backups)  
- File UNlocker (Used to automatically unlock files in use by Arma3Server.exe)  
- Log Compressor (Compresses your old log files in zip containers)  
- Rebooter (Simple script that restarts your servers upon box restart) 
- Repo_Downloader (automatic arma3sync repo downloader)  
 

### Download: ###
[https://bitbucket.org/kamaradski/ahoy-server-tools/downloads](https://bitbucket.org/kamaradski/ahoy-server-tools/downloads)
 
### Installation: ###
Read the included documentation on GoogleDrive
 
### Links: ###
[Downloads](https://bitbucket.org/kamaradski/ahoy-server-tools/downloads)  
[Issue Tracker](https://bitbucket.org/kamaradski/ahoy-server-tools/issues?status=new&status=open)  
[Ahoyworld.co.uk release](http://www.ahoyworld.co.uk/topic/3180-ahoy-server-tools/)  
[Armaholic release](http://www.armaholic.com/page.php?id=26407)  
[kamaradski.com release](http://kamaradski.com/2349/ahoyworld-co-uk-presents-ahoy-server-tools)  
[BIS forums release](http://forums.bistudio.com/showthread.php?181300-Ahoy-Server-Tools)  
   
**Please note:** This is a community project, and we will appreciate all external ideas to improve these scripts or extend it's functionality. Any input can be logged in the Issue Tracker or bigger input can be happily forked and imported with a pull request. These scripts have been tested on Windows 2008 R2, and might need small changes in order to work on other versions of this OS.

**Donations:** We take donations in the form of code-contributions or on our PayPal account via the following donation link:  
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WHH8VTC58XU7G
