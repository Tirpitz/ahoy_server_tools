@echo off
SETLOCAL

REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			repo-downloader.bat
REM Version:		V1.0
REM Author:			Kamaradski 2014
REM Contributers:	none
REM
REM MOD repo downloader written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-





REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

rem Location where i can find WinSCP ftp-client
set winscploc=C:\apps\winscp555\WinSCP.com

rem Location as to where the mods should be downloaded to
set targetdir=C:\Games\A3_EU3_GameNight

rem Server as where the repo FTP is located
set server=repo.ahoyworld.co.uk

set INTERVAL=5
set INTERVAL2=20



REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-

for /L %%A in (%INTERVAL%,-1,0) do ( 
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Repo downloader by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Creating automatic FTP-script in %%A seconds.
echo.
ping localhost -n 2 >nul 
)
> FTP-temp.scr echo.
>>FTP-temp.scr ECHO option batch abort
>>FTP-temp.scr ECHO option confirm off
>>FTP-temp.scr ECHO open ftp://anonymous:anonymous@%server%/modded/
>>FTP-temp.scr ECHO option transfer binary
>>FTP-temp.scr ECHO synchronize local %targetdir%\ -filemask="*.*; |*a3s/" 
>>FTP-temp.scr ECHO close
>>FTP-temp.scr ECHO exit




for /L %%A in (%INTERVAL%,-1,0) do ( 
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Repo downloader by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting download from %server% in %%A seconds.
echo.
ping localhost -n 2 >nul 
)
"%winscploc%" /script="%~dp0\FTP-temp.scr"





for /L %%A in (%INTERVAL2%,-1,0) do ( 
ping localhost -n 2 >nul 
)




for /L %%A in (%INTERVAL%,-1,0) do ( 
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Repo downloader by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Cleaning up in %%A seconds.
echo.
ping localhost -n 2 >nul 
)


TYPE NUL > "%~dp0\FTP-temp.scr"
DEL "%~dp0\FTP-temp.scr"
