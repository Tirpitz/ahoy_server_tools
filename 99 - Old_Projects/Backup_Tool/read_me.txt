// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			Backup_Tool.bat
// Author:			Kamaradski 2014
// Contributers:	none
//
// Dedicated Server Backup script written for Ahoyworld.co.uk
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
(warning: Script is stable, but still in early development and thus not very user friendly)

This script will create a bare minimum backup mirror of your arma server(s), and any 
other content you point it to, in a seperate folder on your hard-drive. This can be used 
to make remote backups with the use of BTsync or Dropbox. This script will also call the log compressor


How:
- Edit the script to make sure all the paths are correct (source and destination)
- Enable or disable the use of log compression (When enabled, make sure the scripts are in the correct folders)


Dependancies:
none


The user can:
- Select source
- Select destination
- Select interval between the backup actions (in order to reduce i/o load
- Select if you want the logs to be compressed


What does Ahoyworld use it for ?
- Twice a week this script will compress our logs, and maka a backup of our servers
- This will than be synced remotely with BTsync, and BTsync is configged to also keep 
	several old versions of the changed files


Contribution:
In case you like to improve this script: Please FORK it on Bitbucket Git, and send us a pull-request