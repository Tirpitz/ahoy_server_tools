@echo off
setlocal

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM ARMA3 remote banlist V2 sync script by Kamaradski for AhoyWorld.co.uk
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                                                      Script variables
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

REM Set Dropbox master ban.txt location (Fullpath)
set dropdir=C:\Dropbox\ARMA_Bansync_V2\Dropmasterban.txt

REM Set Local master ban.txt location (Fullpath)
set bandir=C:\Games\Bansync_V2\Localmasterban.txt

REM Set Local arma3 server ban.txt location (Fullpath)
set locdir1=C:\Games\a3_server1\ban.txt
set locdir2=C:\Games\a3_server2\ban.txt
set locdir3=C:\Games\a3_server3\ban.txt
set locdir4=C:\Games\a3_server4\ban.txt
set locdir5=C:\Games\a3_server5\ban.txt
set locdir6=C:\Games\a3_server_dev_dev\ban.txt
set locdir7=C:\Games\a3_server_dev_stable\ban.txt

REM Set local MPmissions folder for the stable I&A servers & Server names
set locmis1=C:\Games\a3_server1\MPMissions
set locmisname1=a3_Server1

set locmis2=C:\Games\a3_server2\MPMissions
set locmisname2=a3_Server2

REM Set Bansync Logfile location (Fullpath)
set logloc=C:\Dropbox\ARMA_Bansync_V2\Logs\Ban_Synclog_Flagship.txt

REM Set MPMissions Logfile location (Fullpath)
set logloc2=C:\Dropbox\ARMA_Bansync_V2\Logs\MPM_Synclog_Flagship.txt

REM Set REMOTE MPmissions container
set remmis=C:\Dropbox\ARMA_MPMissions

REM Set interval time (seconds):
set runinterval=15

REM Set unique servername where this script is running
set servname=Flagship

REM Set debug messages verbose level 0(off) 1(normal) 2(high) (For testing purposes only)
set debugger=0

REM Set actual file manipulations ON or OFF (For testing purposes only)
set fileman=ON

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                       Script Engine   **DO NOT EDIT BELOW THIS LINE **
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

:start
CLS
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo                 AhoyWorld.co.uk BanSync_V2 by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
if %debugger% GTR 0 echo WARNING: DEBUG MODE IS ON, verbose=%debugger% !
if %fileman% EQU OFF echo WARNING: FILE MANIPULATION IS SWITCHED OFF ! 
if %debugger% GTR 0 echo.

echo Finding File last modified Time and Date
for %%a in ("%dropdir%") do set DropDateStamp=%%~ta
for %%a in ("%bandir%") do set LocDateStamp=%%~ta
if %debugger% GTR 0 echo DEBUG: "%dropdir%" = "%DropDateStamp%"
if %debugger% GTR 0 echo DEBUG: "%bandir%" = "%LocDateStamp%"
echo Checking for changes:...
if "%DropDateStamp%" NEQ "%LocDateStamp%" goto ParseTime 
goto SameDate

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                             Parse file timestamps for exact comparison 
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

:ParseTime
echo Changes detected, performing detailed analysis...
if %debugger% GTR 0 echo DEBUG: Parse Dates and Part of day
for /F "tokens=1,2,3" %%G in ("%DropDateStamp%") do set "DropDate=%%G"  & set "DropTime=%%H" & set "DropAmPm=%%I"
if %debugger% GTR 1 echo DEBUG: Parsing REMOTE timestamp: TIMESTAMP=%DropDateStamp% - DATE=%DropDate% - TIME=%DropTime% - AM-PM=%DropAmPm%
for /F "tokens=1,2,3" %%G in ("%LocDateStamp%") do set "LocDate=%%G" & set "LocTime=%%H" & set "LocAmPm=%%I"
if %debugger% GTR 1 echo DEBUG: Parsing LOCAL timestamp: TIMESTAMP=%LocDateStamp% - DATE=%LocDate% - TIME=%LocTime% - AM-PM=%LocAmPm%
for /F "tokens=1,2,3 delims=/" %%G in ("%DropDate%") do set "DropMonth=%%G" & set "DropDay=%%H" & set "DropYear=%%I" & if %%I LSS 100 set "DropYear=20%%c" 
if %debugger% GTR 1 echo DEBUG: Parsing REMOTE date: MONTH=%DropMonth% - DAY=%DropDay% - YEAR=%DropYear%
for /F "tokens=1,2,3 delims=/" %%G in ("%LocDate%") do set "LocMonth=%%G" & set "LocDay=%%H" & set "LocYear=%%I" & if %%I LSS 100 set "LocYear=20%%c"
if %debugger% GTR 2 echo DEBUG: Parsing LOCAL date: MONTH=%LocMonth% - DAY=%LocDay% - YEAR=%LocYear%
if %debugger% GTR 1 echo.

if %debugger% GTR 0 echo DEBUG: Comparing Dates and Part of day
if %debugger% GTR 1 echo DEBUG: Comparing YEAR: REMOTE=%DropYear% greater then LOCAL=%LocYear% ?
if %DropYear% GTR %LocYear% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing YEAR: REMOTE=%DropYear% less then LOCAL=%LocYear% ?
if %DropYear% LSS %LocYear% goto LocNew
if %debugger% GTR 1 echo DEBUG: Comparing MONTH: REMOTE=%DropMonth% greater then LOCAL=%LocMonth% ?
if %DropMonth% GTR %LocMonth% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing MONTH: REMOTE=%DropMonth% less then LOCAL=%LocMonth% ?
if %DropMonth% LSS %LocMonth% goto LocNew
if %debugger% GTR 1 echo DEBUG: Comparing DAY: REMOTE=%DropDay% greater then LOCAL=%LocDay% ?
if %DropDay% GTR %LocDay% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing DAY: REMOTE=%DropDay% less then LOCAL=%LocDay% ?
if %DropDay% LSS %LocDay% goto LocNew 
if %debugger% GTR 1 echo DEBUG: Comparing DAYPART (AM or PM): REMOTE=%DropAmPm% greater then LOCAL=%LocAmPm% ?
if %DropAmPm% GTR %LocAmPm% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing DAYPART (AM or PM): REMOTE=%DropAmPm% less then LOCAL=%LocAmPm% ?
if %DropAmPm% LSS %LocAmPm% goto LocNew
if %debugger% GTR 1 echo.

if %debugger% GTR 0 echo Parse Times
for /F "tokens=1,2 delims=:" %%G in ("%DropTime%") do set "DropHour=%%G" & set "DropMin=%%H"
if %debugger% GTR 1 echo DEBUG: Parsing REMOTE time: HOUR=%DropHour% - MINUTE=%DropMin%
for /F "tokens=1,2 delims=:" %%G in ("%LocTime%") do set "LocHour=%%G" & set "LocMin=%%H"
if %debugger% GTR 1 echo DEBUG: Parsing LOCAL time: HOUR=%LocHour% - MINUTE=%LocMin%
if %debugger% GTR 1 echo.

if %debugger% GTR 0 echo Comparing Times
if %debugger% GTR 1 echo DEBUG: Comparing HOUR: REMOTE=%DropHour% greater LOCAL=%LocHour%
if %DropHour% GTR %LocHour% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing HOUR: REMOTE=%DropHour% less LOCAL=%LocHour%
if %DropHour% LSS %LocHour% goto LocNew
if %debugger% GTR 1 echo DEBUG: Comparing MINUTES: REMOTE=%DropMin% greater LOCAL=%LocMin%
if %DropMin% GTR %LocMin% goto DropNew
if %debugger% GTR 1 echo DEBUG: Comparing MINUTES: REMOTE=%DropMin% less LOCAL=%LocMin%
if %DropMin% LSS %LocMin% goto LocNew
if %debugger% GTR 1 echo DEBUG: Comparing whole-time: REMOTE=%DropTime% equal LOCAL=%LocTime%
if %DropTime% EQU %LocTime% (
	echo Change detected took place in same minute as current time, skipping sync...
	goto SameDate
)
echo Congratulations: You should never have seen this text :P
goto start


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                                     Run when both files are same date
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
:SameDate
echo No Banlist changes detected, continue to sync the MPMissions files...
GOTO mpmis


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                                      Run when Dropbox files are newer
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
:DropNew
:DropNew
echo REMOTE file changed: Adding changes to local masterlist...
if %fileman% EQU ON copy "%dropdir%" "%bandir%" > NUL
if %fileman% EQU ON del "%locdir1%" & del "%locdir2%" & del "%locdir3%" & del "%locdir4%" & del "%locdir5%" & del "%locdir6%" & del "%locdir7%" > NUL
if %fileman% EQU ON mklink /h "%locdir1%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir2%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir3%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir4%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir5%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir6%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir7%" "%bandir%" > NUL
echo %DATE% %TIME% - %servname%: REMOTE Ban changes synced - Timestamp: %DropDateStamp% >> "%logloc%"
echo BanSync complete, continue to check for updated multiplayer mission files...
GOTO mpmis


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                                        Run when Local files are newer
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
:LocNew
echo LOCAL file changed: Adding changes to Remote masterlist...
if %fileman% EQU ON copy "%bandir%" "%dropdir%" > NUL
if %fileman% EQU ON del "%locdir1%" & del "%locdir2%" & del "%locdir3%" & del "%locdir4%" & del "%locdir5%" & del "%locdir6%" & del "%locdir7%" > NUL
if %fileman% EQU ON mklink /h "%locdir1%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir2%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir3%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir4%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir5%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir6%" "%bandir%" > NUL
if %fileman% EQU ON mklink /h "%locdir7%" "%bandir%" > NUL
echo %DATE% %TIME% - %servname%: LOCAL Ban changes synced - Timestamp: %LocDateStamp% >> "%logloc%"
echo BanSync complete, continue to check for updated multiplayer mission files...
GOTO mpmis

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM                                                Sync MPMissions folder
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
:mpmis
echo.
echo Checking for missing MPMission files to COPY to: %locmisname1% ...
for /f "delims=" %%G in ('dir /b /a-d "%remmis%" 2^>NUL') do if not exist "%locmis1%\%%G" echo Copying: %%G & if %fileman% EQU ON copy "%remmis%\%%G" "%locmis1%\%%G" & echo %DATE% %TIME% - %servname% Copied %%G to %locmis1% >> "%logloc2%"
echo.
echo Checking for missing MPMission files to COPY to: %locmisname2% ...
for /f "delims=" %%G in ('dir /b /a-d "%remmis%" 2^>NUL') do if not exist "%locmis2%\%%G" echo Copying: %%G & if %fileman% EQU ON copy "%remmis%\%%G" "%locmis2%\%%G" & echo %DATE% %TIME% - %servname% Copied %%G to %locmis2% >> "%logloc2%"
echo.
echo Checking for obsolete MPMission files to DELETE from: %locmisname1% ...
for /f "delims=" %%G in ('dir /b /a-d "%locmis1%" 2^>NUL') do if not exist "%remmis%\%%G" echo Deleting: %%G & if %fileman% EQU ON del "%locmis1%\%%G" >NUL 2>&1 & IF EXIST "%locmis1%\%%G" (echo Locked  : %%G) ELSE echo %DATE% %TIME% - %servname% Deleted %%G from %locmis1% >> "%logloc2%"  
echo.
echo Checking for obsolete MPMission files to DELETE from: %locmisname2% ...
for /f "delims=" %%G in ('dir /b /a-d "%locmis2%" 2^>NUL') do if not exist "%remmis%\%%G" echo Deleting: %%G  & if %fileman% EQU ON del "%locmis2%\%%G" >NUL 2>&1 & IF EXIST "%locmis2%\%%G" (echo Locked  : %%G) ELSE echo %DATE% %TIME% - %servname% Deleted %%G from %locmis2% >> "%logloc2%" 
echo.
if %debugger% NEQ 0 ( pause ) else ( timeout /T "%runinterval%" )
GOTO start
